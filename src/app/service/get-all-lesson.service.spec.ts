import { TestBed } from '@angular/core/testing';

import { GetAllLessonService } from './get-all-lesson.service';

describe('GetAllLessonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetAllLessonService = TestBed.get(GetAllLessonService);
    expect(service).toBeTruthy();
  });
});
