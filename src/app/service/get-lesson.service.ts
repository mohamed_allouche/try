import { Injectable } from '@angular/core';
import { Lesson } from '../modules/lesson';

@Injectable({
  providedIn: 'root'
})
export class GetLessonService {
  lessons = [new Lesson(1 , "math" , "mohamed") , new Lesson(2,"physique","houssem") , new Lesson(3,"Histoire","koubaa") ];

  constructor() { }
  getLesson(id){
    return this.lessons.find(x => x.id == id  )
  }
}
