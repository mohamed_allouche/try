import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserServiceService } from 'src/app/service/user-service.service';
import { UserAccount } from 'src/app/modules/user-account';


@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  userChangedSubscription : Subscription;
  user : UserAccount;
  constructor(public userService: UserServiceService) { }
  getUser(id){
    this.userService.getOne(id);
  }
  ngOnInit() {
    this.userChangedSubscription = this.userService.userChanged
                  .subscribe(
                    (user : UserAccount) => {
                      console.log(user);
                      this.user = user;
                    }
                  );
  }

}
