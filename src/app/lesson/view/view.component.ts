import { Component, OnInit } from '@angular/core';
import { GetAllLessonService } from 'src/app/service/get-all-lesson.service';
import { Lesson } from 'src/app/modules/lesson';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  lessons=[];
  constructor( private getAllLesson :GetAllLessonService ) { 
    this.lessons = getAllLesson.getAllLessons();

  }

  ngOnInit() {
  }


}
