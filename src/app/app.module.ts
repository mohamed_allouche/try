import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewComponent } from './lesson/new/new.component';
import { ViewComponent } from './lesson/view/view.component';
import { LessonComponent } from './lesson/lesson.component';
import { EditComponent } from './lesson/edit/edit.component';
import { ExamComponent } from './exam/exam.component';
import { GetAllLessonService } from './service/get-all-lesson.service';
import { GetLessonService } from './service/get-lesson.service';
import {HttpClientModule} from '@angular/common/http'
import { fakeBackendProvider } from './service/fakeBackendInterceptor.interceptor';
import { UserServiceService } from './service/user-service.service';

@NgModule({
  declarations: [
    AppComponent,
    NewComponent,
    ViewComponent,
    LessonComponent,
    EditComponent,
    ExamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [GetAllLessonService,
              GetLessonService,
              fakeBackendProvider,
              UserServiceService,
              

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
