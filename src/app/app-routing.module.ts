import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './lesson/view/view.component';
import { LessonComponent } from './lesson/lesson.component';
import { EditComponent } from './lesson/edit/edit.component';
import { NewComponent } from './lesson/new/new.component';
import { ExamComponent } from './exam/exam.component';

const routes: Routes = [
              {path:'lesson',component:LessonComponent,children:
              [
              {path:'vue',component:ViewComponent},
              {path:'edit',component:EditComponent},
              {path:'new',component:NewComponent}]},
              {path:'exam',component:ExamComponent}
              
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
