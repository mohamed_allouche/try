import { TestBed } from '@angular/core/testing';

import { GetLessonService } from './get-lesson.service';

describe('GetLessonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetLessonService = TestBed.get(GetLessonService);
    expect(service).toBeTruthy();
  });
});
