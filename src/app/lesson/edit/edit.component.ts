import { Component, OnInit } from '@angular/core';
import { GetLessonService } from 'src/app/service/get-lesson.service';
import { Lesson } from 'src/app/modules/lesson';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  lesson: Lesson;
  constructor( private getLesson :GetLessonService ) { 
      }
  Get(id) {
    this.lesson= this.getLesson.getLesson(id)
  }
  ngOnInit() {
  }

}
