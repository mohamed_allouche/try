import { Injectable } from '@angular/core';
import {HttpClient , HttpParams , HttpHeaders,HttpResponse} from '@angular/common/http';
import {GLOBAL} from '../appConfig';
import {Subject} from 'rxjs';
import {UserAccount, user1} from '../modules/user-account';
import {map,catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
private path = `${GLOBAL.apiEndPoint}/users`;
userChanged = new Subject<UserAccount> ();
  constructor( public httpClient : HttpClient) { }
  getOne (id : string) {
    return  this.httpClient.get <UserAccount>(`${this.path}/` + id)
      .pipe(map((user) => {
        return user ;
      }  , catchError (error => {
        return throwError('mochkla');
      }))
      )
      .subscribe (
        (user) => {
          this.userChanged.next(user); 
        }
      )
  }
}